# Cadastro produto

Aplicação web completa com persistência e validação de dados e uma interface de usuário, utilizando Thymeleaf com Bootstrap, Spring Boot e banco de dados MySql. 

Para acessar a aplicação : http://localhost:8080/home

Por favor lembrar de mudar o acesso ao banco de dados no arquivo de propiedade.