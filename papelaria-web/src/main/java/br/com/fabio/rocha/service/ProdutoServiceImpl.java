package br.com.fabio.rocha.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fabio.rocha.dao.ProdutoDao;
import br.com.fabio.rocha.domain.Produto;

@Service
@Transactional
public class ProdutoServiceImpl implements ProdutoService{

    @Autowired
    private ProdutoDao produtotDao;

    @Override
    public void salvar(Produto produto) {
        produtotDao.salvar(produto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Produto> recuperar() {
        return produtotDao.recuperar();
    }

    @Override
    @Transactional(readOnly = true)
    public Produto recuperarPorId(long id) {
        return produtotDao.recuperarPorID(id);
    }

    @Override
    public void atualizar(Produto produto) {
        produtotDao.atualizar(produto);
    }

    @Override
    public void excluir(long id) {
        produtotDao.excluir(id);
    }
	
}
