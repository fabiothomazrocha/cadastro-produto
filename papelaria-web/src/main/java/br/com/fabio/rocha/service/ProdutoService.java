package br.com.fabio.rocha.service;

import java.util.List;

import br.com.fabio.rocha.domain.Produto;

public interface ProdutoService {
	void salvar(Produto produto);

	List<Produto> recuperar();

	Produto recuperarPorId(long id);

	void atualizar(Produto produto);

	void excluir(long id);

}
